// KontoSim
// Von Patrick Leonhardt und Ren� H�usler

// Derzeit auf 25 Konten begrenzt
#include "stdafx.h"
#include <stdlib.h>
//Globals
int ikonten = 0; //Anzahl der bisherigen Konten. 
struct Konten
{
	char name[30];
	char vorname[60];
	float buchungen[25];
	int iB = 0;
	float kontostand;
};
//Funktionen
void buchung(Konten *k, float eingabe)
{
	if ((*k).iB < 24)
	{
		(*k).buchungen[(*k).iB] = eingabe;
		(*k).iB += 1;
	}
	else
	{
		// Verschiebt den Inhalt des Buchungenarrays eines nach hinten. Damit am Ende Platz ist.
		int i = 0;
		for (i = 0; i < 25; i++)
		{
			(*k).buchungen[i] = (*k).buchungen[i + 1];
		}
		(*k).buchungen[24] = eingabe;
	}
}
void einzahlung(Konten *k, float eingabe)
{
	int vergleichswert = eingabe * 100;
	if (vergleichswert >= 0)
	{
		(*k).kontostand += eingabe;
		buchung(k, eingabe);
		printf("\n Einzahlung wurde getaetigt.\n\n");
	}
	else
	{
		printf("\n Einzahlung fehlgeschlagen. Einzahlung von negativen Betraegen nicht moeglch.\n\n");
	}
}
void auszahlung(Konten *k, float eingabe)
{
	int vergleichswert1 = (*k).kontostand * 100;
	int vergleichswert2 = eingabe * 100;
	if (vergleichswert1 >> vergleichswert2)
	{
		(*k).kontostand += -eingabe;
		buchung(k, -eingabe);
		printf("\n Auszahlung wurde getaetigt.\n\n");
	}
	else
	{
		printf("\n Auszahlung nicht moeglich. Kontostand zu niedrig.\n\n");
	}

}
void ueberweisung(Konten *absender, Konten *empf�nger, float eingabe)
{
	int vergleichswert1 = (*absender).kontostand * 100;
	int vergleichswert2 = eingabe * 100;

	if (vergleichswert1 >> vergleichswert2)
	{
		auszahlung(absender, eingabe);
		einzahlung(empf�nger, eingabe);
		printf("\n Ueberweisung wurde getaetigt.\n\n");
	}
	else
	{
		printf("\n Ueberweisung fehlgeschlagen.Kontostand zu niedrig.\n\n");
	}
}
void kontoerstellen(Konten *k)
{
	if (ikonten < 25)
	{
		printf(" Nachname:");
		fgets((*k).name, 60, stdin);
		printf("\n Vorname :");
		fgets((*k).vorname, 30, stdin);
		(*k).kontostand = 0;
		printf("\nIhre Kontonummer: %d\n\n", ikonten);
		int i;
		for (i = 0; i < 25; i++)
		{
			(*k).buchungen[i] = 0;
		}
		ikonten += 1;
	}
	else
	{
		printf("Fehler. Unzureichende Kapazit�ten");
	}
}
void kontoausgabe(Konten *k)
{
	int i;
	printf("\n Vorname.......:%s", (*k).vorname);
	printf(" Nachname......:%s", (*k).name);
	printf(" Kontostand....:%.2f EURO", (*k).kontostand);
	printf("\n Buchungen...... ");
	for (i = 0; i < 25; i++)
	{
		printf("\n .....: %.2f EURO", (*k).buchungen[i]);
	}
}
void speichernNeu(Konten k[])
{
	FILE *file;
	file = fopen("Daten.txt", "w+");

	if (file == NULL)
	{
		printf("Datei konnte NICHT geoeffnet werden.\n");
		printf("Bitte wenden Sie sich an Ihren Administrator.");
	}
	else
	{
		fprintf(file, "%d\n", ikonten);
		int iB;
		int iK;
		for (iK = 0; iK < ikonten; iK++)
		{
			printf("Datei konnte geoeffnet werden.\n");
			fputs((k[iK]).name, file);
			fputs((k[iK]).vorname, file);
			fprintf(file, "%.2f\n", (k[iK]).kontostand);
			fprintf(file, "%d\n", (k[iK]).iB);
			for (iB = 0; iB < 25; iB++)
			{
				fprintf(file, "%.2f:", (k[iK]).buchungen[iB]);
			}
			fprintf(file, "\n");
		}
		fclose(file);

	}

}
void laden(Konten k[])
{
	FILE *file;
	file = fopen("Daten.txt", "r");

	if (file == NULL)
	{
		printf("Datei konnte nicht geoeffnet werden.\n");
		printf("Wenn dies beim ersten Programmstart passiert, ist dies nicht schlimm.");
	}

	else
	{
		fscanf(file, "%d\n", &ikonten);
		int iB;
		int iK;
		for (iK = 0; iK < ikonten; iK++)
		{
			fgets((k[iK]).name, 60, file);
			fgets((k[iK]).vorname, 30, file);
			fscanf(file, "%f\n", &(k[iK]).kontostand);
			fscanf(file, "%d\n", &(k[iK]).iB);
			for (iB = 0; iB < 25; iB++)
			{
				fscanf(file, "%f:", &(k[iK]).buchungen[iB]);
			}
			fscanf(file, "\n");
		}

	}

}
// User interface
int zuKontoverwaltung(int eingabe)
{
	printf(" Zurueck zur Kontoverwaltung: 1\n\n");
	printf("Ihre Eingabe: ");
	scanf_s(" %d", &eingabe);
	getchar();
	if (eingabe == 1)
	{
		return 12;
	}
	else
	{
		return 999;
	}
}
int zuHauptmenue(int eingabe)
{
	printf("  Zurueck zum Hauptmenue: 1\n\n");
	printf("Ihre Eingabe: ");
	scanf_s(" %d", &eingabe);
	getchar();
	if (eingabe == 1)
	{
		return 1;
	}
	else
	{
		return 999;
	}
}
int Kontoverwaltung(int eingabe)
{
	printf("\n\n  Einzahlen  : 1\n  Auszahlen  : 2\n  Ueberweisen: 3\n  Zurueck zum Hauptmenue: 4\n\n");
	printf("Ihre Eingabe: ");
	scanf_s(" %d", &eingabe);
	getchar();
	if (eingabe == 1)
	{
		return 21;
	}
	else if (eingabe == 2)
	{
		return 22;
	}
	else if (eingabe == 3)
	{
		return 23;
	}
	else if (eingabe == 4)
	{
		return 1;
	}
	else
	{
		return 999;
	}
}
int Hauptmenue(int eingabe)
{
	printf("  Neues Konto anlegen: 1\n  Kontoverwaltung    : 2\n  Beenden            : 3\n\n");
	printf("Ihre Eingabe: ");
	scanf_s(" %d", &eingabe);
	getchar();
	if (eingabe == 1)
	{
		return 11;
	}
	else if (eingabe == 2)
	{
		return 9;
	}
	else if (eingabe == 3)
	{
		return 99;
	}
	else
	{
		return 999;
	}
}

int main()
{
	Konten k[25];
	float eing;
	int zustand = 1;
	int lzustand = 0;
	int kontonr = 0, kontonrempf;
	laden(k);

	printf("Willkommen\n\n");
	while (true)
	{
		switch (zustand)
		{

		case  1:
			printf("\n__________Hauptmenue____________________________________________________________\n\n");
			zustand = Hauptmenue(eing);
			printf("\n--------------------------------------------------------------------------------");
			system("cls");
			break;
		case 11:
			printf("\n__________Neues Konto anlegen___________________________________________________\n\n");
			kontoerstellen(&k[ikonten]);
			printf("\n Ihr Konto wurde erstellt\n\n");
			zustand = zuHauptmenue(eing);
			printf("\n--------------------------------------------------------------------------------");
			system("cls");
			break;
		case 9:
			printf("\n__________Kontoverwaltung_______________________________________________________\n\n");
			printf(" Ihre Kontonummer:");
			scanf_s(" %d", &kontonr);
			if (ikonten >= kontonr + 1)
			{
				zustand = 12;
			}
			else
			{
				printf("\n\n  Konto existiert nicht.\n\n");
				zustand = zuHauptmenue(eing);
			}
			system("cls");
			break;
		case 12:
			printf("\n__________Kontoverwaltung_______________________________________________________\n\n");
			printf(" Ihre Kontonummer: %d", kontonr);
			kontoausgabe(&k[kontonr]);
			zustand = Kontoverwaltung(eing);
			printf("\n--------------------------------------------------------------------------------");
			system("cls");
			break;
		case 21:
			printf("\n Einzahlungsbetrag in Euro:");
			scanf_s(" %f", &eing);
			einzahlung(&k[kontonr], eing);
			zustand = zuKontoverwaltung(eing);
			printf("\n--------------------------------------------------------------------------------");
			system("cls");
			break;
		case 22:
			printf("\n Auszahlungsbetrag in Euro:");
			scanf_s(" %f", &eing);
			auszahlung(&k[kontonr], eing);
			zustand = zuKontoverwaltung(eing);
			printf("\n--------------------------------------------------------------------------------");
			system("cls");
			break;
		case 23:
			printf("\n Kontonummer des Zielkontos:");
			scanf_s(" %d", &kontonrempf);
			if (ikonten >= kontonrempf)
			{
				printf("\n Ueberweisungsbetrag in Euro:");
				scanf_s(" %f", &eing);
				ueberweisung(&k[kontonr], &k[kontonrempf], eing);
			}
			else
			{
				printf("\n Fehler. Konto nicht verfuegbar.\n\n");
			}
			zustand = zuKontoverwaltung(eing);
			printf("\n--------------------------------------------------------------------------------");
			system("cls");
			break;
		case 99:
			speichernNeu(k);
			printf("Daten wurden gespeichert.");
			getchar();
			return 0;
			break;
		case 999:
			printf("\n**********Ungueltige Eingabe****************************************************\n\n");
			printf("Zum Hauptmenue: ENTER");
			getchar();
			zustand = 1;
			system("cls");
			break;
		default:
			printf(" ERROR. Anwendung wird geschlossen.\n\n");
			getchar();
			return 0;
			break;
		}

	}
}
Unser Projekt, Kontoführung, wird ein Programm zur Simulation eines Kontos. 
Zu den Funktionalitäten zählen Einzahlungen, Auszahlungen, Überweisungen, sowie das Erstellen von Konten.

Die wohl größte Herausforderung wird sein, die Konten in eine Datei zu schreiben und sie gezielt auszulesen. 
Unser derzeitiger Ansatz sieht vor die Konten mittels Strukturen in Datenfeldern zu schreiben und jene Datenfelder in Datein zu schreiben. 

